import { TestBed, inject } from '@angular/core/testing';

import { HardtestService } from './hardtest.service';

describe('HardtestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HardtestService]
    });
  });

  it('should be created', inject([HardtestService], (service: HardtestService) => {
    expect(service).toBeTruthy();
  }));
});
