import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {User} from './user.model';
import {Router} from '@angular/router';

@Injectable()
export class UserService {
  readonly API_Url = 'https://hardtester.maxqfz.net/api';

  constructor(private http: HttpClient, private router: Router) {
  }

  /** POST request to register new user */
  registerUser(user: User) {
    const url = `${this.API_Url}/account/register`;
    const body: User = {
      email: user.email
    };
    const httpOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/json, text/plain').set('No-Auth', 'True')
    };
    return this.http.post(url, body, httpOptions);
  }

  /** POST request to authenticate user */
  userAuthentication(email, password) {
    const url = `${this.API_Url}/account/auth`;
    const body = {
      'email': email,
      'password': password
    };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'No-Auth': 'True'
      })
    };
    return this.http.post(url, body, {
      observe: 'response',
      headers: new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'})
    });
  }

  /** GET request to logout user */
  userLogout() {
    localStorage.removeItem('userToken');
    this.router.navigate(['/login']);
  }
}
