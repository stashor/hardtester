import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { MessageService } from './message.service';
import { ShortTest } from '../home/test.model';

@Injectable()
export class HardtestService {
  readonly API_Url = 'https://maxqfz.net/hardtester';  // URL to web api

  constructor(private messageService: MessageService, private http: HttpClient) {
  }


  /** GET request to stop test */
  stopHardtest (): Observable<string> {
    const url = `${this.API_Url}/stop`;

    return this.http.get<string>(url).pipe(
      tap((data) => {
        this.log(`fetched data_from_stopHardtest GET ${url}`);
      }),
      catchError(this.handleError<string>(`stopHardtest`))
    );
  }

  /** GET request to start test */
  startHardtest (test: ShortTest): Observable<string> {
    const url = `${this.API_Url}/start`;

    const httpOptions = {
      headers: new HttpHeaders({

      })
    };
    return this.http.post<string>(url, test, httpOptions).pipe(
      tap((data) => {
        this.log(`fetched data_from_startHardtest`);
      }),
      catchError(this.handleError<string>(`startHardtest`))
    );
  }

  // /** GET heroes from the server */
  // POSTstopHardtest (test: Test): Observable<string> {
  //   const url = `${this.API_Url}/start`;
  //   return this.http.post<string>(url, test, httpOptions).pipe(
  //     tap((data) => {
  //       this.log(`fetched data_from_POSTstopHardtest`);
  //     }),
  //     catchError(this.handleError<string>(`POSTstopHardtest`))
  //   );
  // }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add('HardtestService: ' + message);
  }

}
