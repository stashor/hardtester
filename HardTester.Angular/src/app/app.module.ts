import '../polyfills';

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {appRoutes} from './routes';


import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthGuard} from './auth/auth.guard';
import {AuthInterceptor} from './auth/auth.interceptor';

// Material Modules
import {MatButtonModule} from '@angular/material';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSliderModule} from '@angular/material/slider';
import {MatRadioModule} from '@angular/material/radio';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatChipsModule} from '@angular/material/chips';
// import { ReCaptchaModule } from 'angular5-recaptcha';
import {RecaptchaModule} from 'angular-google-recaptcha';
import {ToastrModule} from 'ngx-toastr';


// Components
import {AppComponent} from './app.component';
import {TestformComponent} from './home/testform/testform.component';
import {TeststatsComponent} from './home/teststats/teststats.component';
import {ProfileComponent} from './home/profile/profile.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {WelcomeComponent} from './home/welcome/welcome.component';
import {UserComponent} from './user/user.component';
import {SignInComponent} from './user/sign-in/sign-in.component';
import {HomeComponent} from './home/home.component';
import {SignUpComponent} from './user/sign-up/sign-up.component';

// Services
import {HardtestService} from './shared/hardtest.service';
import {MessageService} from './shared/message.service';
import {UserService} from './shared/user.service';
import {AccountService} from './home/account.service';


@NgModule({
  declarations: [
    AppComponent,
    SignUpComponent,
    UserComponent,
    SignInComponent,
    HomeComponent,
    TestformComponent,
    TeststatsComponent,
    ProfileComponent,
    NotFoundComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    MatButtonModule,
    MatToolbarModule,
    MatSidenavModule,
    MatDividerModule,
    MatInputModule,
    MatFormFieldModule,
    MatSliderModule,
    MatRadioModule,
    MatTableModule,
    MatIconModule,
    MatChipsModule,
    // ReCaptchaModule,
    // siteKey from Google
    RecaptchaModule.forRoot({
      siteKey: '6LfJ31sUAAAAAAGY5EifKI5OafLMMym953mWk8SB'
    }),
  ],
  exports: [
    // MatButtonModule,
    // MatToolbarModule,
    // MatSidenavModule,
    // MatDividerModule,
    // MatInputModule,
    // MatFormFieldModule,
    // MatSliderModule,
    // MatRadioModule,
    // MatTableModule,
    // RouterModule,
    // //ReCaptchaModule
  ],
  providers: [
    UserService,
    AuthGuard,
    HardtestService,
    MessageService,
    AccountService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
