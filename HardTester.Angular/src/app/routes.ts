import {Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {UserComponent} from './user/user.component';
import {SignUpComponent} from './user/sign-up/sign-up.component';
import {SignInComponent} from './user/sign-in/sign-in.component';
import {AuthGuard} from './auth/auth.guard';
import {TestformComponent} from './home/testform/testform.component';
import {TeststatsComponent} from './home/teststats/teststats.component';
import {ProfileComponent} from './home/profile/profile.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {WelcomeComponent} from './home/welcome/welcome.component';

export const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    canActivate: [AuthGuard],
    component: HomeComponent,
    children: [
      {
        path: 'createtest',
        component: TestformComponent,
      },
      {
        path: 'test/:id',
        component: TeststatsComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent,
      },
      {
        path: '',
        component: WelcomeComponent
      }
    ]
  },
  {
    path: 'signup',
    component: UserComponent,
    children: [
      {
        path: '',
        component: SignUpComponent,
      }
    ]
  },
  {
    path: 'login',
    component: UserComponent,
    children: [
      {
        path: '',
        component: SignInComponent,
      }
    ]
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];
