import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/user.service';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  // formControl
  isLoginError: boolean = false;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  constructor(private http: HttpClient, private userService: UserService, private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
  }
  // login user by 'email' and 'password'. Respond: 'success','error'
  onSubmitLogin(email, password) {
    this.userService.userAuthentication(email, password)
      .subscribe(
        (res: HttpResponse<any>) => {
          localStorage.setItem('userToken', res.body.authorization);
          this.router.navigate(['/home']);
        },
        (err: HttpErrorResponse) => {
          this.isLoginError = true;
        });
  }
}
