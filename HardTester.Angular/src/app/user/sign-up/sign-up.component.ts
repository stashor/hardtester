import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { User } from '../../shared/user.model';
import { UserService } from '../../shared/user.service';
//import { ViewChild } from '@angular/core';
//import { ReCaptchaComponent } from 'angular5-recaptcha';
import { FormControl } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  //@ViewChild(ReCaptchaComponent) captcha: ReCaptchaComponent;
  user: User;
  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  // reCaptcha methods for debug and formControl variable 'myRecaptcha'
  myRecaptcha: boolean;
  onScriptLoad() {
    console.log('Google reCAPTCHA loaded and is ready for use!');
  }
  onScriptError() {
    console.log('Something went long when loading the Google reCAPTCHA');
  }


  constructor(private userService: UserService, private toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }
  // reset all fields in form
  resetForm(form?: NgForm) {
    if (form != null) {
      form.reset();
    }
    this.user = {
      email: ''
    };
  }
  // register new user. Returns 'success' + 'error'
  onSubmitRegistration(form: NgForm) {
    this.userService.registerUser(form.value)
      .subscribe(
        res => {
          // TODO:This will never execute, because of bag
          this.resetForm(form);
          this.toastr.success('User registration successful');
        },
        (err: HttpErrorResponse) => {
            this.toastr.error(err.error.message);
        }
      );
  }

}
