import { Component, OnInit, DoCheck, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../shared/user.service';
import { MatTableDataSource } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import {ShortTest, Test, TestListStat} from './test.model';
import { AccountService } from './account.service';
import {TeststatsComponent} from './teststats/teststats.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  constructor(private router: Router, private userService: UserService,
              private accountService: AccountService, private toastr: ToastrService) { }

  tests: TestListStat[];
  timerList;

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  displayedColumns = ['url'];
  dataSource;

  ngOnInit() {
    this.getTestList();
    this.timerList = setInterval(() => this.getTestList(), 1000);
  }

  ngOnDestroy() {
    clearInterval(this.timerList);
  }

  getTestList(): void {
    this.accountService.getTestList()
      .subscribe(tests => {
        this.tests = tests;
        this.dataSource = new MatTableDataSource(this.tests);
      });
  }

  logout() {
    this.userService.userLogout();
  }
}
