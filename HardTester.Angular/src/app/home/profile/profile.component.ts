import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {AccountService} from '../account.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  constructor(private accountService: AccountService, private toastr: ToastrService) {
  }

  ngOnInit() {
    this.getProfileInfo();
  }

  loading = true;
  email: string = '';
  registered: string = '';

  // onSave method from change password Form
  onSave(curP: string, newP: string, repP: string) {
    if (newP === repP) {
      this.accountService.changePassword(curP, newP)
        .subscribe((data: any) => {
            this.toastr.success('Profile has been successfully updated');
          },
          (err: HttpErrorResponse) => {
              this.toastr.error(err.error.message);
          });
    } else {
      this.toastr.error('Error: password and re-password don\'t match');
    }

  }

  // get profile info 'email' and 'created'
  getProfileInfo(): void {
    this.accountService.getProfileInfo()
      .subscribe((data: any) => {
        this.email = data.email;
        this.registered = this.parseDate(new Date(data.registered));
        this.loading = false;
      });
  }
  parseDate(date: Date): string {
    const dat = date.toDateString();
    return dat;
  }

}
