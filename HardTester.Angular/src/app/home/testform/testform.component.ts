import { Component, OnInit } from '@angular/core';
import {objectify} from 'tslint/lib/utils';
import { HardtestService } from '../../shared/hardtest.service';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Test, ShortTest } from '../test.model';
import { AccountService } from '../account.service';
import { Router } from '@angular/router';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-testform',
  templateUrl: './testform.component.html',
  styleUrls: ['./testform.component.css']
})

export class TestformComponent implements OnInit {
  constructor(private hardtestService: HardtestService, private accountService: AccountService, private router: Router) { }

  ngOnInit() {
  }

  // formControl
  urlPattern = "^http(s?):\\/\\/(.+)$";
  urlControl = new FormControl('', [
    Validators.required,
    Validators.pattern(this.urlPattern),
  ]);

  matcher = new MyErrorStateMatcher();

  test = new ShortTest({url: '', sessions: 50, duration: 5, stress_mode: true, start: true});

  types: string[] = [
    'Gradual Test',
    'Stress Test'
  ];

  starter: string[] = [
    'Yes',
    'No'
  ];

  createTest(test: ShortTest): void {
    this.accountService.createTest(test)
      .subscribe((data: any) => {
        console.log(data);
      });
  }
}
