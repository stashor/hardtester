// Test.Model for Test Stats
export class Test {
  id: number;
  url: string;
  sessions: number;
  duration: number;
  stress_mode: boolean;
  created: Date;
  started: Date;
  completed: Date;
  status: string;
}

// Test.Model for Test List
export class TestListStat {
  id: number;
  url: string;
  status: string;
}

// short Test.Model
export class ShortTest {
  url: string;
  sessions: number;
  duration: number;
  stress_mode: boolean;
  start: boolean;

  constructor(obj: ShortTest = {} as ShortTest) {
    const {
      url = '',
      sessions = 0,
      duration = 0,
      stress_mode = true,
      start = true
    } = obj;

    this.url = url;
    this.sessions = sessions;
    this.duration = duration;
    this.stress_mode = stress_mode;
    this.start = start;
  }
}

// chart stats
export class ChartStats {
  time: number;
  count: number;
  answer: number;
}
