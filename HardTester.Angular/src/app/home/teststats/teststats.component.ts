import {Component, OnInit, DoCheck, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {Chart} from 'chart.js';
import {Test, ShortTest, TestListStat, ChartStats} from '../test.model';
import {AccountService} from '../account.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-teststats',
  templateUrl: './teststats.component.html',
  styleUrls: ['./teststats.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class TeststatsComponent implements OnInit, DoCheck, OnDestroy {
  constructor(private router: Router, private toastr: ToastrService,
              private accountService: AccountService, private route: ActivatedRoute,
              private ref: ChangeDetectorRef) {
  }

  // Chart JS
  chart: Chart; // This will hold our chart info
  chartPoint: ChartStats[];
  created: string;
  labels_chart = [];
  session_chart = [];
  answers_chart = [];
  EL: Test[];
  loading = true;
  // chartT: Chart;
  timerId; // Timer for updating the chart
  timerInfo; // Timer for updating the info
  // Mat-Table
  displayedColumns = ['url', 'created', 'sessions', 'duration', 'stress_mode'];
  dataSource;

  ngOnInit() {
    this.checkForExist();
    this.timerId = setInterval(() => this.checkForExist(), 5000);
    this.timerInfo = setInterval(() => this.getTestInfo(), 1000);
    // this.accountService.getTestInfo(this.getID())
    //   .subscribe(data => {
    //     if ((data.status === 'running') || (data.status === 'starting')) {
    //       this.timerId = setInterval(() => this.callbackForInterval(), 5000);
    //     }
    //   });
  }

  ngDoCheck() {
    // this.getTestInfo();
  }

  ngOnDestroy() {
    clearInterval(this.timerId);
    clearInterval(this.timerInfo);
  }

  checkForExist(): void {
    this.accountService.getTestInfo(this.getID())
      .subscribe((data: any) => {
          this.getTestInfo();
          this.getChartInfo();
        },
        (err: HttpErrorResponse) => {
          this.toastr.error(err.error.message);
        });
  }

  getTestInfo(): void {
    this.accountService.getTestInfo(this.getID())
      .subscribe(data => {
        this.loading = false;
        this.created = this.parseDate(new Date(data.created));
        this.EL = [{
          id: data.id,
          url: data.url,
          sessions: data.sessions,
          duration: data.duration,
          stress_mode: data.stress_mode,
          created: data.created,
          started: data.started,
          completed: data.completed,
          status: data.status
        }];
        this.loading = false;
        this.dataSource = new MatTableDataSource(this.EL);
        if ((data.status === 'completed') || (data.status === 'starting')) {
          clearInterval(this.timerId);
        }
        this.ref.markForCheck();
      });
  }

  getChartInfo(): void {
    this.accountService.getChartInfo(this.getID())
      .subscribe(res => {
        this.labels_chart = res.map(a => a.time);
        this.session_chart = res.map(a => a.count);
        this.answers_chart = res.map(a => a.answer);

        this.chartRender();
        this.ref.markForCheck();
      });
  }

  chartRender() {
    Chart.defaults.global.defaultFontFamily = '\'Open Sans\', sans-serif';
    this.chart = new Chart('canvas', {
      type: 'line',
      data: {
        labels: this.labels_chart,
        datasets: [{
          yAxisID: 'S',
          label: 'Sessions',
          data: this.session_chart,
          backgroundColor: 'transparent',
          borderColor: '#07f',
          borderWidth: 1,
          pointBackgroundColor: '#fff',
          pointBorderColor: '#06e',
          pointBorderWidth: 2
        }, {
          yAxisID: 'A',
          label: 'Answer time (s)',
          data: this.answers_chart,
          backgroundColor: 'transparent',
          lineTension: 0.35,
          borderColor: '#2d3',
          borderWidth: 1,
          pointBackgroundColor: '#fff',
          pointBorderColor: '#1c2',
          pointBorderWidth: 2
        }]
      },
      options: {
        responsive: true,
        scales: {
          yAxes: [{
            id: 'S',
            type: 'linear',
            position: 'left',
            ticks: {
              // min: 0,
              // max: 50,
              beginAtZero: true
            }
          }, {
            id: 'A',
            type: 'linear',
            position: 'right',
            ticks: {
              beginAtZero: true
            },
            gridLines: {
              drawOnChartArea: false
            }
          }]
        },
        legend: {
          display: true,
          position: 'bottom',
          labels: {
            // fontColor: 'rgb(255, 99, 132)'
            usePointStyle: true
          }
        }
      }
    });
  }


  stopTest(id: number): void {
    this.accountService.stopTest(id)
      .subscribe((data: any) => {
        // this.toastr.show(data.JSON.stringify());
        clearInterval(this.timerId);
        this.getChartInfo();
      });
  }

  callbackForInterval(): void {
    this.getChartInfo();
  }

  startTest(id: number): void {
    this.accountService.startTest(id)
      .subscribe((data: any) => {
        this.timerId = setInterval(() => this.callbackForInterval(), 5000);
      });
  }

  deleteTest(id: number): void {
    this.accountService.deleteTest(id)
      .subscribe((data: any) => {
        this.router.navigate(['/home']);
      });
  }

  getID(): number {
    const id = +this.route.snapshot.paramMap.get('id');
    return id;
  }

  parseDate(date: Date): string {
    const d = date.getDay();
    const m = date.getMonth() + 1;
    const y = date.getFullYear();
    const h = date.getHours();
    const min = date.getMinutes();
    const s = date.getSeconds();
    return y + '/' + m + '/' + d + ' ' + h + ':' + min + ':' + s;
  }
}
