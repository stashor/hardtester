import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeststatsComponent } from './teststats.component';

describe('TeststatsComponent', () => {
  let component: TeststatsComponent;
  let fixture: ComponentFixture<TeststatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeststatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeststatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
