import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { Test, ShortTest, TestListStat, ChartStats } from './test.model';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class AccountService {
  constructor(private http: HttpClient) { }
  readonly API_Url = 'https://hardtester.maxqfz.net/api';

  /** GET request to retrieve account information */
  getProfileInfo() {
    const url = `${this.API_Url}/account/data`;
    return this.http.get(url);
  }
  /** POST request to change user's password */
  changePassword(password, newpassword) {
    const url = `${this.API_Url}/account/data`;
    const body = {
      'password': password,
      'newpassword': newpassword
    };
    const httpOptions = {
      headers: new HttpHeaders({
      })
    };
    return this.http.post(url, body, httpOptions);
  }
  /** GET request to retrieve test info */
  getTestInfo(id: number): Observable<Test> {
    const url = `${this.API_Url}/tests/${id}`;
    return this.http.get<Test>(url);
  }
  /** GET request to retrieve tests list */
  getTestList(): Observable<TestListStat[]> {
    const url = `${this.API_Url}/tests`;
    return this.http.get<TestListStat[]>(url);
  }
  /** GET request to retrieve chart info */
  getChartInfo(id: number): Observable<ChartStats[]> {
    const url = `${this.API_Url}/tests/${id}/stats`;
    return this.http.get<ChartStats[]>(url);
  }
  /** POST request to add new hardtest */
  createTest(test: ShortTest): Observable<Test> {
    const url = `${this.API_Url}/tests`;
    const body = test;
    const httpOptions = {
      headers: new HttpHeaders({
      })
    };
    return this.http.post<Test>(url, body, httpOptions);
  }
  /** GET request to start test */
  startTest(id: number) {
    const url = `${this.API_Url}/tests/${id}/start`;
    return this.http.get(url);
  }
  /** GET request to stop test */
  stopTest(id: number) {
    const url = `${this.API_Url}/tests/${id}/stop`;
    return this.http.get(url);
  }
  /** GET request to delete test */
  deleteTest(id: number) {
    const url = `${this.API_Url}/tests/${id}/delete`;
    return this.http.get(url);
  }
}
