﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HardTester.Data.Domains
{
    public class Test
    {
        [Key]
        public long Id { get; set; }

        [Required, ForeignKey("User")]
        public string UserEmail { get; set; }

        public virtual User User { get; set; }

        [Required]
        public string URL { get; set; }

        [Required]
        public short Sessions { get; set; }

        [Required]
        public short Duration { get; set; }

        [Required]
        public bool StressMode { get; set; }

        [Required]
        public string Status { get; set; }

        public DateTime TimeCreated { get; set; }

        public DateTime TimeStarted { get; set; }

        public DateTime TimeCompleted { get; set; }
        
    }
}
