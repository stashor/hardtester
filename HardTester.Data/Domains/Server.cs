﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HardTester.Data.Domains
{
    public class Server
    {
        [Required]
        public long Id{ get; set; }

        [Required]
        public string HostName{ get; set; }

        [Required, ForeignKey("Test")]
        public long TestId { get; set; }
        
        public Test Test{ get; set; }

    }
}
