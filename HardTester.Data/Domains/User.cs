﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HardTester.Data.Domains
{
    public class User
    {
        [Key, MaxLength(254, ErrorMessage = "Email must be 254 characters or less"), MinLength(5)]
        public string Email { get; set; }

        [Required, MaxLength(254, ErrorMessage = "Password must be 254 characters or less")]
        public string Password { get; set; }

        [Required]
        public DateTime Registered { get; set; }
    }
}
