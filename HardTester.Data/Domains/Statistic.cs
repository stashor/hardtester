﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HardTester.Data.Domains
{
    public class Statistic
    {
        [Key, ForeignKey("Test")]
        public long TestId { get; set; }

        public virtual Test Test { get; set; }

        [Key]
        public int Second { get; set; }

        public int SessionCount { get; set; }

        public float AnswerTime { get; set; }

    }
}
