﻿//Неймспейсы группируй по источнику, а то я так привык и буду бомбить что ты так не делаешь)
using System;
using System.Text;
using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;

using HardTester.Data.Domains;

namespace HardTester.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
        
        //Здесь прописываются DbSet'ы созданных моделей, что бы они загружались с контекстом (конечно, на практике тут дохуя тонкостей)

        //Название сета - название таблицы + 's'
        public DbSet<Test> Tests { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Server> Servers { get; set; }

        public DbSet<Statistic> Statistics { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Statistic>()
                .HasKey(c => new { c.TestId, c.Second });
        }
    }
}
