﻿using HardTester.Data.Domains;
using System.Linq;

namespace HardTester.Data.IManagers
{
    public interface IStatisticManager
    {
        //Получить статистику теста его Id
        IQueryable<Statistic> GetTestStatistic(long testId);
    }
}
