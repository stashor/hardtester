﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using HardTester.Data.Domains;

namespace HardTester.Data.IManagers
{
    public interface ITestManager
    {
        //Получение всех тестов пользователя по его email
        IQueryable<Test> GetTests(string email);

        //Получение теста по его Id
        IQueryable<Test> GetTest(long testId);
    }
}
