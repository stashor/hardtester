﻿using HardTester.Data.Domains;
using System.Linq;

namespace HardTester.Data.IManagers
{
    public interface IUserManager
    {
        IQueryable<User> GetUsers();

        IQueryable<User> GetUser(string email);

        IQueryable<User> ChangePassword(string email, string password);

        IQueryable<User> AddUser(string email, string password);

    }
}
