﻿using HardTester.Data.Domains;
using System.Linq;

namespace HardTester.Data.Managers
{
    class StatisticManager
    {
        private readonly DataContext dataContext;

        public StatisticManager(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public IQueryable<Statistic> GetStatistics(long testId)
        {
            return dataContext.Statistics.Where(x => x.TestId == testId);
        }
    }
}
