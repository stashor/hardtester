﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

using HardTester.Data.Domains;
using HardTester.Data.IManagers;

namespace HardTester.Data.Managers
{
    public class TestManager : ITestManager
    {
        //По-хорошему менеджеры нужно делить на секурные и публичные и ограничивать к ним и к их методам доступ. 
        //Ну, скорее всего, так и придется делать когда начнем писать авторизацию.
        private readonly DataContext dataContext;

        public TestManager(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public IQueryable<Test> GetTest(long id)
        {
            return dataContext.Tests.Where(x => x.Id == id);
        }

        //Получение всех тестов пользователя по его email
        public IQueryable<Test> GetTests(string email)
        {
            return dataContext.Tests.Where(x => x.UserEmail == email);
        }

        //Добавить сервер к тесту
        public IQueryable<Test> AddServer(long testId, string hostName)
        {
            var server = new Server
            {
                HostName = hostName,
                TestId = testId
            };

            dataContext.Add(server);
            dataContext.SaveChanges();

            return server.Test as IQueryable<Test>;
        }

        // Получить все сервера, прикрепленные к тесту по id теста
        public IQueryable<Server> GetServers(long id)
        {
            return dataContext.Servers.Where(x => x.TestId == id);
        }

        //Получить статистику по тесту по id теста
        public IQueryable<Statistic> GetTestStatistic(long id)
        {
            return dataContext.Statistics.Where(x => x.TestId == id);
        }

        public async Task RemoveServer(long id)
        {
            var server = dataContext.Servers.Where(x => x.Id == id);

            dataContext.Remove(server);
            await dataContext.SaveChangesAsync();
        }

        // Удалить все сервера, прикрепленные к тесту по id теста
        public async Task RemooveAllServers(long id)
        {
            var servers = dataContext.Servers.Where(x => x.TestId == id);

            dataContext.RemoveRange(servers);
            await dataContext.SaveChangesAsync();
        }
    }
}
