﻿using HardTester.Data.Domains;
using HardTester.Data.IManagers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace HardTester.Data.Managers
{
    public class UserManager : IUserManager
    {
        private readonly DataContext dataContext;

        public UserManager(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public IQueryable<User> GetUsers()
        {
            return dataContext.Users;
        }


        public IQueryable<User> GetUser(string email)
        {
            return dataContext.Users.Where(x => x.Email == email);
        }

        public IQueryable<User> ChangePassword(string email, string password)
        {
            var user = dataContext.Users.Where(x => x.Email == email);
            user.Single().Password = password;

            dataContext.SaveChangesAsync();

            return user as IQueryable<User>;
        }

        public IQueryable<User> AddUser(string email, string password)
        {
            var user = new User
            {
                Email = email,
                Password = password,
                Registered = DateTime.Now
            };

            dataContext.Add(user);
            dataContext.SaveChangesAsync();

            return user as IQueryable<User>;
        }
    }
}
