﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using HardTester.Data.Domains;
using HardTester.API.Models;

using AutoMapper;

namespace HardTester.API.Config
{
    public static class AutoMapperConfig
    {
        public static void Init()
        {
            //Это я для примера. Когда поле в поле - не надо создавать профиль

            Mapper.Initialize(
                cfg => {
                    cfg.CreateMap<Test, TestShortModel>()
                    .ForMember(d => d.URL, opt => opt.MapFrom(s => s.Id));
                });
        }
    }
}
