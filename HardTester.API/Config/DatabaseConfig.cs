﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using HardTester.Data;

namespace HardTester.API.Config
{
    public static class DatabaseConfig
    {
        public static void AddDataContext(this IServiceCollection services, string connection)
        {
            services.AddDbContext<DataContext>(conf => conf.UseSqlServer(connection));
        }
    }
}
