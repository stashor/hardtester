﻿using HardTester.Data.IManagers;
using HardTester.Data.Managers;
using Microsoft.Extensions.DependencyInjection;

namespace HardTester.API.Config
{
    public static class DependeciesConfig
    {
        //Создание инстансов для DI
        public static void AddDataServices(this IServiceCollection services)
        {
            services.AddTransient<ITestManager, TestManager>();
            services.AddTransient<IUserManager, UserManager>();
        }
    }
}
