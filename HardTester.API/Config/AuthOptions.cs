﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;

namespace HardTester.API.Config
{
    public static class AuthOptions
    {
        //TODO: вынести в appsettings
        private const string KEY = "~Y=`^vHDqcX6j5,3";

        public const string ISSUER = "HardTester";
        public const string AUDIENCE = "https://localhost:444/";
        public static DateTime LIFETIME = DateTime.Now.AddHours(24); //срок жизни токена в секундах (сутки)

        public static SymmetricSecurityKey SymmetricSecurityKey
        {
            get
            {
                return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
            }
        }

        public static string SystemSecretPhrase
        {
            get
            {
                return "KH!KbY+8WtqluNiosNRCI=";
            }
        }
    }
}
