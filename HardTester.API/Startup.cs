﻿using HardTester.API.Config;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace HardTester.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public object AuthOptions { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Инициализируем автомаппер 
            AutoMapperConfig.Init();

            // Добавление контекста базы данных, строка тянется из appsettings.json
            services.AddDataContext(Configuration.GetConnectionString("HardTesterContext"));

            // Добавления настроек DI
            services.AddDataServices();

            //Авторизация на JWT
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Config.AuthOptions.ISSUER,
                        ValidAudience = Config.AuthOptions.AUDIENCE,
                        IssuerSigningKey = Config.AuthOptions.SymmetricSecurityKey
                    };
                });

            services.AddMvc();
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                // При ошибках будем выводим полный стактрейс
                app.UseDeveloperExceptionPage();
            }

            //Используем авторизацию
            app.UseAuthentication();

            app.UseMvc();

            // Указываем что запускать будем веб из wwwroot
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseCors(builder => builder.WithOrigins("http://localhost:4200"));
        }
    }
}
