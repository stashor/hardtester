﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HardTester.API.Config;
using HardTester.API.Models;
using HardTester.Data.IManagers;
using HardTester.Services;
using Microsoft.AspNetCore.Authorization;

namespace HardTester.API.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class TestsController : Controller
    {
        ITestManager TestManager { get; set; }

        public TestsController(ITestManager testManager)
        {
            TestManager = testManager;
        }

        [HttpGet]
        public IActionResult GetAllTests()
        {
            var tests = TestManager.GetTests("maxqfz@gmail.com");//User.Identity.Name);
            return Ok(tests);
        }
        
        [HttpGet("{id}")]
        public IActionResult GetTest(int id)
        {
            var test = TestManager.GetTest(id).Single();
            if (test == null)
                return BadRequest(new ResultModel("Non-existent test id"));
            if (test.UserEmail != User.Identity.Name)
                return Forbid();
            return Ok(new TestModel(test.Id, test.URL, test.Sessions, test.Duration, test.StressMode, 
                test.Status, test.TimeCreated, test.TimeStarted, test.TimeCompleted));
        }
        
        [HttpPost]
        public void CreateTest([FromBody]string value)
        {

        }
        
        [HttpPatch("{id}")]
        public void ChangeTestState(int id)
        {

        }
        
        [HttpDelete("{id}")]
        public void DeleteTest(int id)
        {

        }
        
        [HttpGet("{id}/stats")]
        public IActionResult Statistics(int id)
        {
            List<StatsModel> stats = new List<StatsModel>();
            Random r = new Random();
            int time = r.Next(70, 1800), sessions = r.Next(1, 3);
            for (int i = 0, j = 0; i < time; i += 5, j += sessions)
                stats.Add(new StatsModel((short)i, j, 0.5 + r.NextDouble()));
            return Ok(stats);
        }
    }
}
