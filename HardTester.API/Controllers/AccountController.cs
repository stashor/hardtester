﻿using HardTester.API.Config;
using HardTester.API.Models;
using HardTester.Data.IManagers;
using HardTester.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HardTester.API.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class AccountController : Controller
    {
        IUserManager UserManager { get; set; }

        public AccountController(IUserManager userManager)
        {
            UserManager = userManager;
        }

        [HttpPost("auth")]
        public async Task<IActionResult> Login([FromBody]AccountModel account)
        {
            if (String.IsNullOrEmpty(account.Email))
                return BadRequest(new ResultModel("No e-mail specified"));
            if (String.IsNullOrEmpty(account.Password))
                return BadRequest(new ResultModel("No password specified"));

            var user = await UserManager.GetUser(account.Email).SingleOrDefaultAsync();
            if (user == null || !PasswordManager.VerifyHashedPassword(user.Password, account.Password))
                return Unauthorized();

            //Creating and returning token
            var token = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                claims: new[] { new Claim(ClaimTypes.Name, user.Email) },
                expires: AuthOptions.LIFETIME,
                signingCredentials: new SigningCredentials(
                    AuthOptions.SymmetricSecurityKey, SecurityAlgorithms.HmacSha256
                )
            );
            string authorization = "Bearer " + new JwtSecurityTokenHandler().WriteToken(token);
            return Ok(new TokenModel(authorization));
        }
        
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]AccountModel account)
        {
            if (String.IsNullOrEmpty(account.Email))
                return BadRequest(new ResultModel("No e-mail specified"));

            var dbUser = await UserManager.GetUser(account.Email).SingleOrDefaultAsync();
            if (dbUser != null)
                return StatusCode(409, new ResultModel("User with such email exists"));

            // Генерируем пароль указанной длины и хэшируем
            string password = "12345678"; //PasswordManager.GeneratePassword(10);
            string hashedPassword = PasswordManager.HashPassword(password);

            var user = UserManager.AddUser(account.Email, hashedPassword);

            /*
             * Здесь надо отправить пароль пользователю на почту. 
             * Когда будет сделано, можно расскоментировать генератор случайного пароля.
             */

            return Ok(new ResultModel());
        }

        [Authorize]
        [HttpGet("data")]
        public async Task<IActionResult> Information()
        {
            var user = await UserManager.GetUser(User.Identity.Name).SingleOrDefaultAsync();
            if (user == null)
                return Unauthorized();
            return Ok(new AccountInfoModel(user.Email, user.Registered));
        }

        [Authorize]
        [HttpPost("data")]
        public async Task<IActionResult> PasswordChange([FromBody]AccountPasswordChangeModel change)
        {
            var user = await UserManager.GetUser(User.Identity.Name).SingleOrDefaultAsync();
            if (user == null)
                return Unauthorized();
            if (!PasswordManager.VerifyHashedPassword(user.Password, change.Password))
                return StatusCode(403, new ResultModel("Wrong current password"));

            var hashedPassword = PasswordManager.HashPassword(change.NewPassword);
            UserManager.ChangePassword(user.Email, hashedPassword);
            
            return Ok(new ResultModel());
        }
    }
}