﻿using System;

namespace HardTester.API.Models
{
    public class TestModel
    {
        public long Id { get; set; }
        public string URL { get; set; }
        public short Sessions { get; set; }
        public short Duration { get; set; }
        public bool Stress_Mode { get; set; }
        public string Status { get; set; }
        public DateTime Created { get; set; }
        public DateTime Started { get; set; }
        public DateTime Completed { get; set; }

        public TestModel(long id, string url, short sessions, short duration, bool stress_mode, 
            string status, DateTime created, DateTime started, DateTime completed)
        {
            Id = id;
            URL = url;
            Sessions = sessions;
            Duration = duration;
            Stress_Mode = stress_mode;
            Status = status;
            Created = created;
            Started = started;
            Completed = completed;
        }
    }
}