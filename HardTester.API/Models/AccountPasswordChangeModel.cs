﻿namespace HardTester.API.Models
{
    public class AccountPasswordChangeModel
    {
        public string Password { get; set; }

        public string NewPassword { get; set; }
    }
}
