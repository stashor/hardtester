﻿namespace HardTester.API.Models
{
    public class TestShortModel
    {
        public long Id { get; set; }
        public string URL { get; set; }
        public string Status { get; set; }
    }
}