﻿namespace HardTester.API.Models
{
    public class TokenModel
    {
        public string Authorization;

        public TokenModel(string token)
        {
            Authorization = token;
        }
    }
}
