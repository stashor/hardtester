﻿using System;

namespace HardTester.API.Models
{
    public class AccountInfoModel
    {
        public string Email { get; set; }

        public DateTime Registered { get; set; }

        public AccountInfoModel(string email, DateTime registered)
        {
            Email = email;
            Registered = registered;
        }
    }
}
