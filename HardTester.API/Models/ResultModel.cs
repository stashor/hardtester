﻿namespace HardTester.API.Models
{
    public class ResultModel
    {
        public bool Success { get; set; }
        public string Message { get; set; }

        public ResultModel()
        {
            Success = true;
        }

        public ResultModel(string message)
        {
            Message = message;
        }

        public ResultModel(bool success, string message)
        {
            Success = success;
            Message = message;
        }
    }
}
