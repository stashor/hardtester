﻿namespace HardTester.API.Models
{
    public class StatsModel
    {
        public short Time { get; set; }
        public int Count { get; set; }
        public double Answer { get; set; }

        public StatsModel(short time, int count, double answer)
        {
            Time = time;
            Count = count;
            Answer = answer;
        }
    }
}