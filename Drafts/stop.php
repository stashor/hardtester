<?php

/*
 * STOP PENTEST
 */

const XTOKEN = '...';

header('Content-Type: application/json');

function delete_server($ctid) {
	$ch = curl_init('https://api.vscale.io/v1/scalets/'.$ctid);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Token: '.XTOKEN));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	if ($result != '') 
	{
		$server = json_decode($result, true);
		if(isset($server['ctid']))
	        return;
		else
			die('{"success":false,"message":"Ошибка получения информации об удалённом сервере"}');
	}
	else
		die('{"success":false,"message":"Ошибка удаления нагрузочного сервера"}');
}

$ids = file_get_contents('server_ids.txt');
if($ids === false || $ids == '')
	die('{"success":false,"message":"Нагрузочные серверы отсутствуют"}');
$ids = explode(PHP_EOL, $ids);
foreach($ids as $id)
{
    if($id != '')
        delete_server($id);
}
unlink('server_ids.txt');
echo '{"success":true,"message":"Нагрузочное тестирование остановлено"}';
?>