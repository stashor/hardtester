﻿namespace HardTester.Services.Vscale
{
    class NewServerModel
    {
        public string Make_From { get; set; }
        public string Rplan { get; set; }
        public bool Do_Start { get; set; }
        public string Name { get; set; }
        public int[] Keys { get; set; }
        public string Location { get; set; }

        public NewServerModel()
        {
            Make_From = "297604ae-4fa1-4733-b8d6-b86c45e0232b";
            Rplan = "small";
            Do_Start = true;
            Name = "Server";
            Keys = new int[] { 20614 };
            Location = "spb0";
        }
    }
}
