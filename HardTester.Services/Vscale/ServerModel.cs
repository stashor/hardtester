﻿namespace HardTester.Services.Vscale
{
    class IpAddress
    {
        public string Gateway { get; set; }
        public string NetMask { get; set; }
        public string Address { get; set; }
    }

    class ServerModel
    {
        public int CtId { get; set; }
        public string Status { get; set; }
        public IpAddress Public_Address { get; set; }
    }
}
