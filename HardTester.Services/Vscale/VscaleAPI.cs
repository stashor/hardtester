﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HardTester.Services.Vscale
{
    class VscaleAPI
    {
        const string URL = "https://api.vscale.io/v1/scalets/";

        public async Task<int> CreateServer()
        {
            using (var client = new HttpClient())
            {
                NewServerModel newServer = new NewServerModel();
                var requestJson = JsonConvert.SerializeObject(newServer);
                var requestData = new StringContent(requestJson, Encoding.UTF8, "application/json");
                var response = await client.PostAsync(URL, requestData);
                var responseData = await response.Content.ReadAsStringAsync();
                ServerModel server = JsonConvert.DeserializeObject<ServerModel>(responseData);
                return server.CtId;
            }
        }

        public async Task<string> GetServerStatus(int ctid)
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(URL + ctid.ToString());
                var responseData = await response.Content.ReadAsStringAsync();
                ServerModel server = JsonConvert.DeserializeObject<ServerModel>(responseData);
                return server.Status;
            }
        }

        public async Task<String> GetServerIp(int ctid)
        {
            using (var client = new HttpClient())
            {
                ServerModel server;
                do
                {
                    var response = await client.GetAsync(URL + ctid.ToString());
                    var responseData = await response.Content.ReadAsStringAsync();
                    server = JsonConvert.DeserializeObject<ServerModel>(responseData);
                }
                while (server.Public_Address != null);
                return server.Public_Address.Address;
            }
        }

        public async Task<bool> DeleteServer(int ctid)
        {
            using (var client = new HttpClient())
            {
                var response = await client.DeleteAsync(URL + ctid.ToString());
                var responseData = await response.Content.ReadAsStringAsync();
                ServerModel server = JsonConvert.DeserializeObject<ServerModel>(responseData);
                if (server.Status == "deleted")
                    return true;
                else
                    return false;
            }
        }
    }
}
