﻿using Renci.SshNet;

namespace HardTester.Services.SSH
{
    class SshConnection
    {
        SshClient client;

        SshConnection(string ip, string keypath)
        {
            var privkey = new PrivateKeyAuthenticationMethod("root", new PrivateKeyFile(keypath));
            var connectionInfo = new ConnectionInfo(ip, 22, "root", privkey);
            client = new SshClient(connectionInfo);
        }

        public string SendCommand(string command)
        {
            if(!client.IsConnected)
                client.Connect();
            return client.CreateCommand(command).Execute();
        }
    }
}
